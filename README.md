# Multiple Choice Questions (MCQs) Service

This service provides APIs to

- Create MCQ
- Query MCQ
- Update MCQ
- Delete MCQ

This implementation supports mongodb for persistent data storage. The service is intended to be used through containers. It is mandatory to use db-host as alias for mongodb container.
The service also supports uploading of images multiple choice options.	Use questionDiagrams request parameter to upload images for question where as answerDiagrams request parameter to upload images for answers.


Test sevice using curl. Following are the examples.

- curl -v  -F  d="0003“  -F  subject="java"    -F    question="What is the output of this program, please tell?“  -F choices="2 2, 3 3, Runtime Error, Compilation Error"  http://HOST:PORT/mcqs

- curl http://HOST:PORT/mcqs/java 

- curl -X DELETE http://HOST:PORT/mcqs/java
