/**
 * 
 */
package org.spl.oes.common.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;


/**
 * @author sane
 *
 */

public class AppConfig
{
    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);
    
    private static final String MONGO_TEMPLATE = "mongoTemplate";
    
    private static final String MONGO_CONFIG_FILE = "mongo-config.xml";
    
    private static final String CUSTOM_CONFIG = "customConfiguration";
    
    private static final String CUSTOM_CONFIG_FILE = "custom-config.xml";
    
    private static GenericXmlApplicationContext mongoConfigContext;
    
    private static GenericXmlApplicationContext custConfigContext;
    
    
    static
    {
        logger.info("Initializing AppConfig");
        custConfigContext = new GenericXmlApplicationContext(CUSTOM_CONFIG_FILE);
        mongoConfigContext = new GenericXmlApplicationContext(MONGO_CONFIG_FILE);
    }
    /*
     * A piece of code is thread-safe if it only manipulates shared data structures 
     * in a manner that guarantees safe execution by multiple threads at the same time.
     */
    
    /*MongoTemplate is thread safe. MongoClient has internal connection pooling.
     *For most applications, you should have one MongoClient instance for the entire JVM.
     */
    
    private static MongoTemplate mongoTemplate;
    
    private static CustomConfiguration customConfiguration;
    


    public static MongoTemplate getMongoTemplate()
    {
        if(mongoTemplate == null)
        {
            logger.info("mongoTemplate is null");
            if(mongoConfigContext == null)
            {
                logger.info("mongoConfigContext is null");
                mongoConfigContext = new GenericXmlApplicationContext(MONGO_CONFIG_FILE); 
                logger.info("mongoConfigContext : {}", mongoConfigContext);
            }
            else
            {
                mongoTemplate = (MongoTemplate) mongoConfigContext.getBean(MONGO_TEMPLATE);
            }
        }
        logger.info("mongoTemplate : {}", mongoTemplate);
        return mongoTemplate;
    }

    public static CustomConfiguration getCustomConfiguration()
    {
        if(customConfiguration == null)
        {
            logger.info("customConfiguration is null");
            
            if(custConfigContext == null)
            {
                logger.info("custConfigContext is null");
                custConfigContext = new GenericXmlApplicationContext(CUSTOM_CONFIG_FILE);
                logger.info("custConfigContext : {}", custConfigContext);
            }
            else
            {
                customConfiguration = (CustomConfiguration) custConfigContext.getBean(CUSTOM_CONFIG);
            }
        }
        logger.info("customConfiguration : {}", customConfiguration);
        return customConfiguration;
    }
}
