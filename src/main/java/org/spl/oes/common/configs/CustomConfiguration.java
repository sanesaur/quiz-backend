package org.spl.oes.common.configs;

public class CustomConfiguration
{
    private String db;
    
    private String subjectPattern;
    
    private String subjectCollectionPrefix;
    
    private String questionPaperCollectionPrefix;

    public String getDb()
    {
        return db;
    }

    public void setDb(String db)
    {
        this.db = db;
    }

    public String getSubjectPattern()
    {
        return this.subjectPattern;
    }

    public void setSubjectPattern(String subjectPattern)
    {
        this.subjectPattern = subjectPattern;
    }
    
    public String getSubjectCollectionPrefix()
    {
        return this.subjectCollectionPrefix;
    }
    
    public void setSubjectCollectionPrefix(String subjectCollectionPrefix)
    {
        this.subjectCollectionPrefix = subjectCollectionPrefix;
    }
    
    public String getQuestionPaperCollectionPrefix()
    {
        return this.questionPaperCollectionPrefix;
    }
    
    public void setQuestionPaperCollectionPrefix(String questionPaperCollectionPrefix)
    {
        this.questionPaperCollectionPrefix = questionPaperCollectionPrefix;
    }
}
