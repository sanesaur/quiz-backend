/**
 * 
 */
package org.spl.oes.common.persistence.mongo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.common.configs.CustomConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sane
 *
 */
public abstract class AbstractMongoService
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractMongoService.class);
    
    public static final String MONGO_DB = "Mongo";
    
    private Pattern pattern;
    
    private boolean validatePattern(String value) throws Exception
    {
        CustomConfiguration customConfiguration = AppConfig.getCustomConfiguration();
        
        if(this.pattern == null)
        {
            this.pattern = Pattern.compile(customConfiguration.getSubjectPattern());
        }
        
        Matcher matcher = pattern.matcher(value);
        
        if(!matcher.matches())
        {
            throw new Exception("Only subjects having [a-zA-Z0-9_] characters are allowed.");
        }
        
        return true;
    }
    
    protected String getCollectionName(String prefix, String subject ) throws Exception
    {
        String collectionName = prefix + subject.toLowerCase();
        
        if(this.validatePattern(collectionName))
        {
            logger.info("Generated collection name : {}", collectionName);
            return collectionName;
        }
        else
        {
            throw new Exception("Subject name " + subject + "not comply with pattern " + this.pattern.pattern());
        }
       
    }
    
    protected Pattern getPattern()
    {
        return this.pattern;
    }
    
}
