package org.spl.oes.subs.controllers;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spl.oes.subs.delegates.SubjectDelegate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubjectController
{
    private static final Logger logger = LoggerFactory.getLogger(SubjectController.class);

    @RequestMapping(value = "/subs", method = RequestMethod.POST)
    private ResponseEntity<?> create(@RequestParam("subject") String subject) throws Exception
    {
        SubjectDelegate subjectDelegate = new SubjectDelegate(subject);
        String createdSubject = subjectDelegate.create(subject);
        return new ResponseEntity(subject, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/subs", method = RequestMethod.GET)
    private ResponseEntity<?> listSubjects()
    {
        SubjectDelegate subjectDelegate = new SubjectDelegate(null);
        Collection<String> allSubjects = subjectDelegate.getAllSubjects();
        return new ResponseEntity(allSubjects, HttpStatus.OK);
    }
    
    @RequestMapping (value = "/subs", params = "subject", method = RequestMethod.GET)
    private ResponseEntity<?> searchSubjectContainingCharacters(@RequestParam("subject") String subject)
    {
        
        return null;
    }    

    @RequestMapping (value = "/subs/{subject}", params = "newname", method = RequestMethod.PUT)
    private ResponseEntity<?> updateSubject(@PathVariable("subject") String subject,
                                        @RequestParam("newname") String newname) throws Exception
    {
        SubjectDelegate subjectDelegate = new SubjectDelegate(subject);
        String updatedName = subjectDelegate.update(subject, newname);
        return new ResponseEntity(updatedName, HttpStatus.OK);
    }
    
    @RequestMapping (value = "/subs/{subject}", method = RequestMethod.DELETE)
    private ResponseEntity<?> deleteSubject(@PathVariable("subject") String subject)
    {
        SubjectDelegate subjectDelegate = new SubjectDelegate(subject);
        boolean deleteSubject = subjectDelegate.deleteSubject(subject);
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }
    
}
