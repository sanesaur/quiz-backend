/**
 * 
 */
package org.spl.oes.subs.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.common.persistence.mongo.AbstractMongoService;
import org.spl.oes.mcqs.services.MultiChoiceQuestionServiceLookUp;
import org.spl.oes.mcqs.services.MultipleChoiceQuestionMongoService;
import org.spl.oes.mcqs.services.MultipleChoiceQuestionService;

/**
 * @author sane
 *
 */
public class SubjectServiceLookUp
{
    private static final Logger logger = LoggerFactory.getLogger(SubjectServiceLookUp.class);
    
    
    public static SubjectService getSubjectDbService()
    {
        SubjectService subjectService = null;
        
        if(AbstractMongoService.MONGO_DB.equalsIgnoreCase(AppConfig.getCustomConfiguration().getDb()))
        {
            subjectService = new SubjectMongoService();
            
            logger.info("SubjectMongoDbService object is created.");
        }
        
        return subjectService;
    }
}
