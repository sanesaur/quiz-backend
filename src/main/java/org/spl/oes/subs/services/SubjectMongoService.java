/**
 * 
 */
package org.spl.oes.subs.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.common.persistence.mongo.AbstractMongoService;
import org.spl.oes.mcqs.models.MultipleChoiceQuestion;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;

/**
 * @author sane
 *
 */
public class SubjectMongoService extends AbstractMongoService 
                                    implements SubjectService
{
    static final private Logger logger = LoggerFactory.getLogger(SubjectMongoService.class);
    
    private static final String SUBJECT_PREFIX = AppConfig.getCustomConfiguration().getSubjectCollectionPrefix();
    
    @Override
    public String create(String subject) throws Exception
    {
        String collectionName = this.getCollectionName(SUBJECT_PREFIX, subject);

        MongoTemplate mongoTemplate = AppConfig.getMongoTemplate();
        
        if(!mongoTemplate.collectionExists(collectionName))
        {
            logger.info("Collection {} does not exist.",collectionName);
            
            mongoTemplate.createCollection(collectionName);
            /*
             * Build text index for multiple choice question
             */
            TextIndexDefinitionBuilder textIndexBuilder = TextIndexDefinition.builder()
                    .onField(MultipleChoiceQuestion.QUESTION);
            mongoTemplate.indexOps(collectionName).ensureIndex(textIndexBuilder.build());
            return collectionName;
        }
        else
        {
            logger.info("Collection {} exist.",collectionName);
            throw new Exception("Collection " + collectionName + " exists.");
        }            
  
    }

    @Override
    public String update(String subject, String newsubject) throws Exception
    {
        String oldCollectionName = SUBJECT_PREFIX + subject.toLowerCase();
        String newCollectionName = SUBJECT_PREFIX + subject.toLowerCase();
        
        MongoTemplate mongoTemplate = AppConfig.getMongoTemplate();
        
       
        if(mongoTemplate.collectionExists(oldCollectionName) &&
                !mongoTemplate.collectionExists(newCollectionName))
        {                
            String newCollection = this.create(newsubject);
            
            if(newCollection != null)
            {
                List<MultipleChoiceQuestion> questionsInOldCollection = mongoTemplate.findAll(MultipleChoiceQuestion.class, oldCollectionName);
                Collection<MultipleChoiceQuestion> questionsInNewCollection = mongoTemplate.insert(questionsInOldCollection, newCollectionName);
                mongoTemplate.dropCollection(oldCollectionName);
                return newCollection;
            }
            else
            {
                logger.info("New collection {} cannot be created", newCollection);
                throw new Exception("New collection " + newCollection + " cannot be created.");
            }
        }
        else
        {            
            logger.info("Either {} does not exist or {} already exist", oldCollectionName, newCollectionName);
            throw new Exception("Either "+ oldCollectionName +" does not exist or "+ newCollectionName + " already exist");
        }
    }

    @Override
    public Collection<String> getAllSubjects()
    {
        MongoTemplate mongoTemplate = AppConfig.getMongoTemplate();
        Set<String> collectionNames = mongoTemplate.getCollectionNames();
        
        mongoTemplate.getCollectionNames().forEach( (collectionName)-> {
            if (!collectionName.startsWith(SUBJECT_PREFIX))
            {
                collectionNames.remove(collectionName);
            }
        } );
        return collectionNames;
    }

    @Override
    public boolean deleteSubject(String subject)
    {
        String collectionName = SUBJECT_PREFIX + subject.toLowerCase();        
        MongoTemplate mongoTemplate = AppConfig.getMongoTemplate();        
        mongoTemplate.dropCollection(collectionName);
        
        return true;
    }

}
