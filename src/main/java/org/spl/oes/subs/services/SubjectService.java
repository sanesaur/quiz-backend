/**
 * 
 */
package org.spl.oes.subs.services;

import java.util.Collection;

/**
 * @author sane
 *
 */
public interface SubjectService
{
    public String create(String subject) throws Exception;
    
    public String update(String subject, String newSubject) throws Exception;
    
    public Collection<String> getAllSubjects();
    
    public boolean deleteSubject(String subject);
}
