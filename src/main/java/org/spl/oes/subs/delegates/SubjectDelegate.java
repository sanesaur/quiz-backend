package org.spl.oes.subs.delegates;

import java.util.Collection;

import org.spl.oes.subs.services.SubjectService;
import org.spl.oes.subs.services.SubjectServiceLookUp;

public class SubjectDelegate
{
    private SubjectService subjectService;
    
    public SubjectDelegate(String subject)
    {
        this.subjectService = SubjectServiceLookUp.getSubjectDbService();
    }
    
    public String create(String subject) throws Exception
    {
        return this.subjectService.create(subject);
    }
    
    public String update(String subject, String newSubject) throws Exception
    {
        return this.subjectService.update(subject, newSubject);
    }
    
    public Collection<String> getAllSubjects()
    {
        return this.subjectService.getAllSubjects();
    }
    
    public boolean deleteSubject(String subject)
    {
        return this.subjectService.deleteSubject(subject);
    }
}
