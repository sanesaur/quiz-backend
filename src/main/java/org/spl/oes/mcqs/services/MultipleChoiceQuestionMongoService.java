/**
 * 
 */
package org.spl.oes.mcqs.services;

import java.util.List;

import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.mcqs.models.MultipleChoiceQuestion;
import org.spl.oes.mcqs.services.MultipleChoiceQuestionService;
import org.spl.oes.common.persistence.mongo.AbstractMongoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Component;
import com.mongodb.client.result.DeleteResult;

/**
 * @author sane
 *
 */
@Component
public class MultipleChoiceQuestionMongoService extends AbstractMongoService
                                        implements MultipleChoiceQuestionService
{
    static final private Logger logger = LoggerFactory.getLogger(MultipleChoiceQuestionMongoService.class);
    
    private static final String COLLECTION_PREFIX = AppConfig.getCustomConfiguration().getSubjectCollectionPrefix();
        
    @Override
    public MultipleChoiceQuestion create(String subject, MultipleChoiceQuestion multipleChoiceQuestion)
                                            throws Exception 
    {
        logger.info("Create question of {}", subject);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        logger.info("Question : {}", multipleChoiceQuestion.getQuestion());
        MultipleChoiceQuestion insertedMultipleChoiceQuestion = null;
        
        if(AppConfig.getMongoTemplate().collectionExists(collectionName))
        {
            logger.info("Collection " + collectionName + " does not exist.");
        }
        insertedMultipleChoiceQuestion = AppConfig.getMongoTemplate().insert(multipleChoiceQuestion, collectionName);
        logger.info("Inserted question: {}", insertedMultipleChoiceQuestion);
      
        
        return insertedMultipleChoiceQuestion;
    }

    @Override
    public List<MultipleChoiceQuestion> getAllQuestions(String subject) throws Exception
    {   
        logger.info("Get all questions of {}", subject);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        
        List<MultipleChoiceQuestion> multipleChoiceQuestions = AppConfig.getMongoTemplate().findAll(MultipleChoiceQuestion.class, collectionName);
        
        logger.info("All questions: {}", multipleChoiceQuestions);
        
        return multipleChoiceQuestions;
    }
    
    @Override
    public MultipleChoiceQuestion findQuestionById(String subject, String id) throws Exception
    {
        logger.info("Find question of {} by {}", subject, id);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        
        logger.info("Get question for id: {}", id);
        MultipleChoiceQuestion multipleChoiceQuestion = AppConfig.getMongoTemplate().findById(id, MultipleChoiceQuestion.class, collectionName);
        logger.info("Question for id: {} is {}", id, multipleChoiceQuestion);
        return multipleChoiceQuestion;
    }
    
    /*
     * This is a text search.
     */
    
    @Override
    public List<MultipleChoiceQuestion> findQuestionsContainingPhrase(String subject, String phrase)  throws Exception
    {
        logger.info("Find question of {} having phrase {}", subject, phrase);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        
        TextCriteria textCriteria =  TextCriteria.forDefaultLanguage().matchingAny(phrase).caseSensitive(false);
        TextQuery textQuery = TextQuery.queryText(textCriteria).sortByScore();
        List<MultipleChoiceQuestion> multipleChoiceQuestions = AppConfig.getMongoTemplate().find(textQuery, MultipleChoiceQuestion.class, collectionName);
        logger.info("Questions with pattern : {} are {}", phrase, multipleChoiceQuestions);
        return multipleChoiceQuestions;
        
    }
    
    @Override
    public MultipleChoiceQuestion updateQuestion(String subject, MultipleChoiceQuestion multipleChoiceQuestion)  throws Exception
    {
        logger.info("Update question of {} : {}", subject, multipleChoiceQuestion);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        
        Query query = new Query(Criteria.where(MultipleChoiceQuestion.ID).is(multipleChoiceQuestion.getId()));
        AppConfig.getMongoTemplate().findAndReplace(query, multipleChoiceQuestion, collectionName);
        
        List<MultipleChoiceQuestion> updatedMultipleChoiceQuestions = AppConfig.getMongoTemplate().find(query, MultipleChoiceQuestion.class, collectionName);
        
        MultipleChoiceQuestion replacedMultipleChoiceQuestion = null;
        
        if(updatedMultipleChoiceQuestions != null && updatedMultipleChoiceQuestions.size() > 0)
        {
            replacedMultipleChoiceQuestion = updatedMultipleChoiceQuestions.get(0);
        }
        
        logger.info("Updated question : {}", replacedMultipleChoiceQuestion);
        return replacedMultipleChoiceQuestion;
    }
    
    @Override
    public boolean deleteAllQuestions(String subject)  throws Exception
    {
        logger.info("Delete questions of {}", subject);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        
        Query query = new Query();
        DeleteResult deleteResult = AppConfig.getMongoTemplate().remove(query, collectionName);  
        boolean wasAcknowledged = deleteResult.wasAcknowledged();
        logger.info("Are questions deleted ? : {}", wasAcknowledged);
        return wasAcknowledged;
    }
    
    @Override
    public boolean deleteQuestion(String subject, String id)  throws Exception
    {
        logger.info("Delete question of {} having {}", subject, id);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        logger.info("Collection Name : {}", collectionName);
        
        Query query = new Query(Criteria.where(MultipleChoiceQuestion.ID).is(id));
        
        List<MultipleChoiceQuestion> multipleChoiceQuestions = AppConfig.getMongoTemplate().find(query, MultipleChoiceQuestion.class, collectionName);
        
        
        if(multipleChoiceQuestions != null && multipleChoiceQuestions.size() > 0)
        {
            MultipleChoiceQuestion multipleChoiceQuestion = multipleChoiceQuestions.get(0);
            logger.info("Question to be deleted : {}", multipleChoiceQuestion);
            DeleteResult remove = AppConfig.getMongoTemplate().remove(multipleChoiceQuestion, collectionName);
            return remove.wasAcknowledged();
        }
        
        return false;
    }

}
