/**
 * 
 */
package org.spl.oes.mcqs.services;

import java.util.List;

import org.spl.oes.mcqs.models.MultipleChoiceQuestion;


/**
 * @author sane
 *
 */
public interface MultipleChoiceQuestionService
{
    public MultipleChoiceQuestion create(String subject, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception;
    
    public List<MultipleChoiceQuestion> getAllQuestions(String subject) throws Exception;
    
    public MultipleChoiceQuestion findQuestionById(String subject, String id) throws Exception;
    
    public List<MultipleChoiceQuestion> findQuestionsContainingPhrase(String subject, String phrase) throws Exception;
    
    public MultipleChoiceQuestion updateQuestion(String subject, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception;
    
    public boolean deleteAllQuestions(String subject) throws Exception;
    
    public boolean deleteQuestion(String subject, String id) throws Exception;
}
