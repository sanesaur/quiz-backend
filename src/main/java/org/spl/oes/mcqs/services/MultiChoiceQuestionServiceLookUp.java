/**
 * 
 */
package org.spl.oes.mcqs.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spl.oes.mcqs.services.MultipleChoiceQuestionMongoService;
import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.common.persistence.mongo.AbstractMongoService;


/**
 * @author sane
 *
 */

public class MultiChoiceQuestionServiceLookUp
{
    private static final Logger logger = LoggerFactory.getLogger(MultiChoiceQuestionServiceLookUp.class);
    
    public static MultipleChoiceQuestionService getMultiChoiceQuestionDbService()
    {
        MultipleChoiceQuestionService multipleChoiceQuestionDbService = null;
        
        if(AbstractMongoService.MONGO_DB.equalsIgnoreCase(AppConfig.getCustomConfiguration().getDb()))
        {
            multipleChoiceQuestionDbService = new MultipleChoiceQuestionMongoService();
            
            logger.info("MultipleChoiceQuestionMongoDbService object is created.");
        }
        
        return multipleChoiceQuestionDbService;
    }
}
