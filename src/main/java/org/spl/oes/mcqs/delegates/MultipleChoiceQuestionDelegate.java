/**
 * 
 */
package org.spl.oes.mcqs.delegates;

import java.util.List;

import org.spl.oes.mcqs.models.MultipleChoiceQuestion;
import org.spl.oes.mcqs.services.MultiChoiceQuestionServiceLookUp;
import org.spl.oes.mcqs.services.MultipleChoiceQuestionService;

/**
 * @author sane
 *
 */
public class MultipleChoiceQuestionDelegate
{
    private MultipleChoiceQuestionService multipleChoiceQuestionService;
    
    public MultipleChoiceQuestionDelegate(String subject) throws Exception
    {
        this.multipleChoiceQuestionService = MultiChoiceQuestionServiceLookUp.getMultiChoiceQuestionDbService();
    }
    
    public MultipleChoiceQuestion create(String subject, MultipleChoiceQuestion multipleChoiceQuestion)  throws Exception
    {
        return this.multipleChoiceQuestionService.create(subject, multipleChoiceQuestion);
    }
    
    public List<MultipleChoiceQuestion> getAllQuestions(String subject) throws Exception
    {
        return this.multipleChoiceQuestionService.getAllQuestions(subject);
    }
    
    public MultipleChoiceQuestion getQuestionById(String subject, String id) throws Exception
    {
        return this.multipleChoiceQuestionService.findQuestionById(subject, id);
    }
    
    public List<MultipleChoiceQuestion> getQuestionsContainingPhrase(String subject, String phrase) throws Exception
    {
        return this.multipleChoiceQuestionService.findQuestionsContainingPhrase(subject, phrase);
    }
    
    public MultipleChoiceQuestion updateQuestion(String subject, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception
    {
        return this.multipleChoiceQuestionService.updateQuestion(subject, multipleChoiceQuestion);
    }
    
    public boolean deleteAllQuestions(String subject) throws Exception
    {
        return this.multipleChoiceQuestionService.deleteAllQuestions(subject);
    }
    
    public boolean deleteQuestion(String subject, String id) throws Exception
    {
        return this.multipleChoiceQuestionService.deleteQuestion(subject, id);
    }
}
