package org.spl.oes.qps.models;

import java.util.List;

import org.spl.oes.mcqs.models.MultipleChoiceQuestion;

public class QuestionPaper 
{
    public String id;
    
    private String setId;
    
    private String calendarYear;
    
    private String academicYear;
    
    private String semester;
    
    private List<MultipleChoiceQuestion> multipleChoiceQuestions;
    
    public static final String ID = "id";
    
    public static final String SET_ID = "setId";
    
    public static final String CALENDAR_YEAR = "calendarYear";
    
    public static final String ACADEMIC_YEAR = "academicYear";
    
    public static final String SEMESTER = "semester";
    
    public static final String MULTIPLE_CHOICE_QUESTIONS = "multipleChoiceQuestions";
    
    public static final String MCQ_QUESTION = MULTIPLE_CHOICE_QUESTIONS + "." + MultipleChoiceQuestion.QUESTION;
    
    public static final String MCQ_ID = MULTIPLE_CHOICE_QUESTIONS + "." + MultipleChoiceQuestion.ID;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getSetId()
    {
        return setId;
    }
    
    public void setSetId(String setId)
    {
        this.setId = setId;
    }
    
    public String getCalendarYear()
    {
        return calendarYear;
    }
    
    public void setCalendarYear(String calendarYear)
    {
        this.calendarYear = calendarYear;
    }
    
    public String getAcademicYear()
    {
        return academicYear;
    }
    
    public void setAcademicYear(String academicYear)
    {
        this.academicYear = academicYear;
    }
    
    public String getSemester()
    {
        return semester;
    }
    
    public void setSemester(String semester)
    {
        this.semester = semester;
    }
    
    public List<MultipleChoiceQuestion> getMultipleChoiceQuestions()
    {
        return multipleChoiceQuestions;
    }
    
    public void setMultipleChoiceQuestions(List<MultipleChoiceQuestion> multipleChoiceQuestions)
    {
        this.multipleChoiceQuestions = multipleChoiceQuestions;
    }
}
