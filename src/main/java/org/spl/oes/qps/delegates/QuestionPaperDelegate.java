/**
 * 
 */
package org.spl.oes.qps.delegates;

import java.util.List;

import org.spl.oes.mcqs.delegates.MultipleChoiceQuestionDelegate;
import org.spl.oes.mcqs.models.MultipleChoiceQuestion;
import org.spl.oes.qps.models.QuestionPaper;
import org.spl.oes.qps.services.QuestionPaperService;
import org.spl.oes.qps.services.QuestionPaperServiceLookUp;



/**
 * @author sane
 *
 */
public class QuestionPaperDelegate
{
    private QuestionPaperService questionPaperService;
    
    public QuestionPaperDelegate() throws Exception
    {
        this.questionPaperService = QuestionPaperServiceLookUp.getQuestionPaperService();
    }
 
    public QuestionPaper create(String subject, QuestionPaper questionPaper) throws Exception
    {
        return this.questionPaperService.create(subject, questionPaper);
    }
    
    public List<QuestionPaper> getAllQuestionPapers(String subject) throws Exception
    {
        return this.questionPaperService.findAllQuestionPapers(subject);
    }
    
    public List<QuestionPaper> getQuestionPaperById(String subject, String id) throws Exception
    {
        return this.questionPaperService.findQuestionPaperById(subject, id);
    }
    
    public List<QuestionPaper> getQuestionPapersContainingQuestionsContainingPhrase(String subject, String phrase) throws Exception
    {
        return this.questionPaperService.findQuestionPapersContainingQuestionsContainingPhrase(subject, phrase);
    }
    
    public List<QuestionPaper> getQuestionPapers(String subject, QuestionPaper questionPaper) throws Exception
    {
        return this.questionPaperService.findQuestionPapers(subject, questionPaper);
    }
    
    public QuestionPaper updateQuestion(String subject, QuestionPaper questionPaper) throws Exception
    {
        return this.questionPaperService.updateQuestionPaper(subject, questionPaper);
    }
    
    public boolean deleteAllQuestionPapers(String subject) throws Exception
    {
        return this.questionPaperService.deleteAllQuestionPapers(subject);
    }
    
    public boolean deleteQuestionPaper(String subject, String id) throws Exception
    {
        return this.questionPaperService.deleteQuestionPaper(subject, id);
    }
    
    public QuestionPaper addMultipleChoiceQuestion(String subject, String id, String questionId) throws Exception
    {
        MultipleChoiceQuestionDelegate  multipleChoiceQuestionDelegate = new MultipleChoiceQuestionDelegate(subject);
        MultipleChoiceQuestion multipleChoiceQuestion = multipleChoiceQuestionDelegate.getQuestionById(subject, questionId);
        return this.questionPaperService.addMultipleChoiceQuestion(subject, id, multipleChoiceQuestion);
    }
    
    public QuestionPaper removeMultipleChoiceQuestion(String subject, String id, String questionId) throws Exception
    {
        MultipleChoiceQuestionDelegate  multipleChoiceQuestionDelegate = new MultipleChoiceQuestionDelegate(subject);
        MultipleChoiceQuestion multipleChoiceQuestion = multipleChoiceQuestionDelegate.getQuestionById(subject, questionId);
        return this.questionPaperService.removeMultipleChoiceQuestion(subject, id, multipleChoiceQuestion);
    }
    
    public List<MultipleChoiceQuestion> getAllMultipleChoiceQuestions(String subject, String id) throws Exception
    {
        return this.questionPaperService.findAllMultipleChoiceQuestions(subject, id);
    }
}
