package org.spl.oes.qps.services;

import java.util.List;

import org.spl.oes.mcqs.models.MultipleChoiceQuestion;
import org.spl.oes.qps.models.QuestionPaper;


public interface QuestionPaperService
{
    public QuestionPaper create(String subject, QuestionPaper questionPaper) throws Exception;
    
    public List<QuestionPaper> findAllQuestionPapers(String subject) throws Exception;
    
    public List<QuestionPaper> findQuestionPaperById(String subject, String id) throws Exception;
    
    public List<QuestionPaper> findQuestionPapersContainingQuestionsContainingPhrase(String subject, String phrase) throws Exception;
    
    public List<QuestionPaper> findQuestionPapers(String subject, QuestionPaper questionPaper) throws Exception;
    
    public QuestionPaper updateQuestionPaper(String subject, QuestionPaper questionPaper) throws Exception;
    
    public boolean deleteAllQuestionPapers(String subject) throws Exception;
    
    public boolean deleteQuestionPaper(String subject, String id) throws Exception;
    
    public QuestionPaper addMultipleChoiceQuestion(String subject, String id, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception;
    
    public QuestionPaper removeMultipleChoiceQuestion(String subject, String id, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception;
    
    public List<MultipleChoiceQuestion> findAllMultipleChoiceQuestions(String subject, String id) throws Exception;
}
