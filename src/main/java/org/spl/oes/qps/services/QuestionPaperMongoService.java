package org.spl.oes.qps.services;

import java.util.ArrayList;
import java.util.List;

import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.mcqs.models.MultipleChoiceQuestion;
import org.spl.oes.common.persistence.mongo.AbstractMongoService;
import org.spl.oes.qps.models.QuestionPaper;
import org.spl.oes.qps.services.QuestionPaperService;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.CompoundIndexDefinition;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;


public class QuestionPaperMongoService extends AbstractMongoService
                                implements QuestionPaperService
{

    static final private Logger logger = LoggerFactory.getLogger(QuestionPaperMongoService.class);

    private static final String COLLECTION_PREFIX = AppConfig.getCustomConfiguration().getQuestionPaperCollectionPrefix();

    private void init(String subject) throws Exception
    {
        logger.info("Initialize service for {}", subject);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        
        MongoTemplate mongoTemplate = AppConfig.getMongoTemplate();
        
        if(!mongoTemplate.collectionExists(collectionName))
        {
            logger.info("Collection {} does not exist.",collectionName);
            
            mongoTemplate.createCollection(collectionName);
            /*
             * Build text index for multiple choice question
             */
            TextIndexDefinitionBuilder textIndexBuilder = TextIndexDefinition.builder()
                    .onField(MultipleChoiceQuestion.QUESTION);
            mongoTemplate.indexOps(collectionName).ensureIndex(textIndexBuilder.build());
            
            /*
             * Compound index for calendarYear, academicYear, Semester
             */
            
            Index setIdIndex = new Index(QuestionPaper.SET_ID, Direction.ASC);
            mongoTemplate.indexOps(collectionName).ensureIndex(setIdIndex);
       
            Document calendaryear_academicyear_semester_document = new Document();
            calendaryear_academicyear_semester_document.append(QuestionPaper.CALENDAR_YEAR, 1)
                        .append(QuestionPaper.ACADEMIC_YEAR, 1)
                        .append(QuestionPaper.SEMESTER, 1);
            CompoundIndexDefinition calendaryear_academicyear_semester_index = new CompoundIndexDefinition(calendaryear_academicyear_semester_document);
            
            mongoTemplate.indexOps(collectionName).ensureIndex(calendaryear_academicyear_semester_index);
            
            /*
             * Compound index for academicYear, Semester
             */
            Document academicyear_semester_document = new Document();
            academicyear_semester_document.append(QuestionPaper.ACADEMIC_YEAR, 1)
                        .append(QuestionPaper.SEMESTER, 1);
            CompoundIndexDefinition academicyear_semester_index = new CompoundIndexDefinition(academicyear_semester_document);
            mongoTemplate.indexOps(collectionName).ensureIndex(academicyear_semester_index);

        }
        else
        {
            logger.info("Collection {} exist.",collectionName);            
        }
    }
    
    @Override
    public QuestionPaper create(String subject, QuestionPaper questionPaper) throws Exception
    {
        logger.info("Create question paper of {} : {}", subject, questionPaper);
        
        this.init(subject);
                
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        logger.info("Question : {}", questionPaper.getSetId() , questionPaper.getCalendarYear(), questionPaper.getAcademicYear(), questionPaper.getSemester());
        
        Criteria criteria = Criteria.where(QuestionPaper.SET_ID).is(questionPaper.getSetId())
                        .and(QuestionPaper.CALENDAR_YEAR).is(questionPaper.getCalendarYear())
                        .and(QuestionPaper.ACADEMIC_YEAR).is(questionPaper.getAcademicYear())
                        .and(QuestionPaper.SEMESTER).is(questionPaper.getSemester());
        
        Query query = new Query(criteria);
                
        List<QuestionPaper> questionPapers = AppConfig.getMongoTemplate().find(query, QuestionPaper.class, collectionName);
        
        QuestionPaper insertedQuestionPaper = null;
        
        if(questionPapers != null && questionPapers.size() >= 1)
        {
            logger.error("Unable to create. Question paper \"{}\" already exists.", questionPapers);
            
            throw new DuplicateKeyException("Unable to create. Question \"" + 
                    questionPapers + "\" already exists.");            
        }
        else
        {
            insertedQuestionPaper = AppConfig.getMongoTemplate().insert(questionPaper, collectionName);
            logger.info("Inserted question: {}", insertedQuestionPaper);
        }        
        
        return insertedQuestionPaper;
    }

    @Override
    public List<QuestionPaper> findQuestionPaperById(String subject, String id) throws Exception
    {
        logger.info("Find question paper of {} : {} ",  subject, id);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        Criteria criteria = Criteria.where(QuestionPaper.SET_ID).is(id);
        
        Query query = new Query(criteria);
        
        List<QuestionPaper> questionPapers = AppConfig.getMongoTemplate().find(query, QuestionPaper.class, collectionName);
        
        logger.info("Question papers for : {} is {}", id, questionPapers);
        
        return questionPapers;
    }
    
    @Override
    public List<QuestionPaper> findQuestionPapers(String subject, QuestionPaper questionPaper) throws Exception
    {
        logger.info("Find question papers of {} : {}", subject, questionPaper);
        String setId = questionPaper.getSetId();
        String calendarYear = questionPaper.getCalendarYear();
        String academicYear = questionPaper.getAcademicYear();
        String semester = questionPaper.getSemester();
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        logger.info("Question paper for {} : {} {} {} {}",  collectionName, setId, calendarYear, 
                academicYear, semester);
        
        
        Criteria criteria = Criteria.where(QuestionPaper.SET_ID).is(setId)
                        .and(QuestionPaper.CALENDAR_YEAR).is(calendarYear)
                        .and(QuestionPaper.ACADEMIC_YEAR).is(academicYear)
                        .and(QuestionPaper.SEMESTER).is(semester);
        
        Query query = new Query(criteria);
        
        List<QuestionPaper> questionPapers = AppConfig.getMongoTemplate().find(query, QuestionPaper.class, collectionName);
        
        logger.info("Question papers for : {} {} {} {} is {}", setId, calendarYear, academicYear, semester, questionPapers);
        
        return questionPapers;
    }

    @Override
    public List<MultipleChoiceQuestion> findAllMultipleChoiceQuestions(String subject, String id) throws Exception
    {
        logger.info("Find all questions of {} for {}", subject, id);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        List<QuestionPaper> questionPapers = findQuestionPaperById(collectionName, id);
        
        List<MultipleChoiceQuestion> multipleChoiceQuestions = new ArrayList<MultipleChoiceQuestion>();
        
        for(QuestionPaper questionPaper : questionPapers)
        {
            List<MultipleChoiceQuestion> multipleChoiceQuestionsInQuestionPaper = questionPaper.getMultipleChoiceQuestions();
            multipleChoiceQuestions.addAll(multipleChoiceQuestions);
        }
        
        logger.info("Multiple choice questions for {} are {}",  id, multipleChoiceQuestions);
        
        return multipleChoiceQuestions;
    }

    @Override
    public List<QuestionPaper> findAllQuestionPapers(String subject) throws Exception
    {
        logger.info("Find all question papers for {}", subject);
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        return AppConfig.getMongoTemplate().findAll(QuestionPaper.class, collectionName);
    }
    
    @Override
    public QuestionPaper updateQuestionPaper(String subject, QuestionPaper questionPaper) throws Exception
    {
        logger.info("Update question paper of {} : {}", subject, questionPaper);
        Query query = new Query(Criteria.where(QuestionPaper.ID).is(questionPaper.getId()));
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        AppConfig.getMongoTemplate().findAndReplace(query, questionPaper, collectionName);
        
        List<QuestionPaper> updatedQuestionPapers = AppConfig.getMongoTemplate().find(query, QuestionPaper.class, collectionName);
        
        QuestionPaper replacedQuestionPaper = null;
        
        if(updatedQuestionPapers != null && updatedQuestionPapers.size() > 0)
        {
            replacedQuestionPaper = updatedQuestionPapers.get(0);
        }
        
        logger.info("Updated question paper : {}", replacedQuestionPaper);
        return replacedQuestionPaper;
    }

    @Override
    public QuestionPaper addMultipleChoiceQuestion(String subject, String id, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception
    {
        logger.info("Add multiple choice question {} in question paper of {}: {}", multipleChoiceQuestion, id, subject);
        Query query = new Query(Criteria.where(QuestionPaper.ID).is(id));
        
        Update update = new Update();
        update.push(QuestionPaper.MULTIPLE_CHOICE_QUESTIONS, multipleChoiceQuestion);
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        UpdateResult updateMulti = AppConfig.getMongoTemplate().updateMulti(query, update, collectionName);
        
        QuestionPaper modifiedQuestionPaper = null;
        
        List<QuestionPaper> modifiedQuestionPapers = findQuestionPaperById(subject, id);
        
        if(modifiedQuestionPapers != null && modifiedQuestionPapers.size() == 1)
        {
            modifiedQuestionPaper = modifiedQuestionPapers.get(0);
        }
        
        logger.info("{} question papers updated.",modifiedQuestionPaper);

        return modifiedQuestionPaper;
    }
    
    @Override
    public QuestionPaper removeMultipleChoiceQuestion(String subject, String id, MultipleChoiceQuestion multipleChoiceQuestion) throws Exception
    {
        logger.info("Remove questions of {} for {}", subject, id);
        String multipleChoiceQuestionId = multipleChoiceQuestion.getId();

        Query query = new Query(Criteria.where(QuestionPaper.MCQ_ID).is(multipleChoiceQuestionId)
                .andOperator(Criteria.where(QuestionPaper.ID).is(id)));
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        DeleteResult remove = AppConfig.getMongoTemplate().remove(query, MultipleChoiceQuestion.class, collectionName);
        
        logger.info("Is question removed ? {}", remove.wasAcknowledged());
        
        QuestionPaper modifiedQuestionPaper = null;
        List<QuestionPaper> modifiedQuestionPapers = findQuestionPaperById(subject, id);
        
        if(modifiedQuestionPapers != null && modifiedQuestionPapers.size() == 1)
        {
            modifiedQuestionPaper = modifiedQuestionPapers.get(0);
        }
        
        logger.info("{} question papers updated.",modifiedQuestionPaper);

        return modifiedQuestionPaper;
    }
    

    @Override
    public boolean deleteAllQuestionPapers(String subject) throws Exception
    {
        logger.info("Delete all the questions of {}", subject);
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        AppConfig.getMongoTemplate().dropCollection(collectionName);
        return true;
    }

    @Override
    public boolean deleteQuestionPaper(String subject, String id) throws Exception
    {
        logger.info("Delete question paper of {} : {}", subject, id);
        Query query = new Query(Criteria.where(QuestionPaper.ID).is(id));
        
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        
        List<QuestionPaper> questionPapers = AppConfig.getMongoTemplate().find(query, QuestionPaper.class, collectionName);
        
        if(questionPapers != null && questionPapers.size() > 0)
        {
            QuestionPaper questionPaper = questionPapers.get(0);
            logger.info("Question paper to be deleted : {}", questionPaper);
            DeleteResult remove = AppConfig.getMongoTemplate().remove(questionPaper, collectionName);
            return remove.wasAcknowledged();
        }
        
        return false;
    }

    @Override
    public List<QuestionPaper> findQuestionPapersContainingQuestionsContainingPhrase(String subject, String phrase) throws Exception
    {
        logger.info("Delete question papers of {} containing phrase : {}", subject, phrase);
        Query query = new Query(Criteria.where(QuestionPaper.MCQ_QUESTION).regex("*" + phrase + "*", "i"));
        String collectionName = this.getCollectionName(COLLECTION_PREFIX, subject);
        List<QuestionPaper> questionPapers = AppConfig.getMongoTemplate().find(query, QuestionPaper.class, collectionName);
        logger.info("Question papers with questions having pattern : {} are {}", phrase, questionPapers);
        return questionPapers;
    }
}
