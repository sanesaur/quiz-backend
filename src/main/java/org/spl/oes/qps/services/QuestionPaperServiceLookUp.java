package org.spl.oes.qps.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spl.oes.common.configs.AppConfig;
import org.spl.oes.common.persistence.mongo.AbstractMongoService;
import org.spl.oes.qps.services.QuestionPaperMongoService;

public class QuestionPaperServiceLookUp
{
    public static final Logger logger = LoggerFactory.getLogger(QuestionPaperServiceLookUp.class);

    public static QuestionPaperService getQuestionPaperService()
    {
        QuestionPaperService questionPaperService = null;
        
        if(AbstractMongoService.MONGO_DB.equalsIgnoreCase(AppConfig.getCustomConfiguration().getDb()))
        {
            questionPaperService = new QuestionPaperMongoService();
            
            logger.info("MultipleChoiceQuestionMongoDbService object is created.");
        }
        
        return questionPaperService;
    }

}
