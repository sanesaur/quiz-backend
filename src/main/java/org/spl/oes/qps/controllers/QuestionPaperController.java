/**
 * 
 */
package org.spl.oes.qps.controllers;

import java.io.IOException;
import java.util.List;

import org.spl.oes.qps.delegates.QuestionPaperDelegate;
import org.spl.oes.qps.models.QuestionPaper;
import org.spl.oes.common.util.CustomError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author sane
 *
 */

/*
 * If @RestController is missing HTTP Request are not dispatched to the controller.
 * status : 404, message : No message available, error : Not found, timestamp : XXX
 * path : /qps is returned as response.
 */

@RestController
public class QuestionPaperController
{
    private static final Logger logger = LoggerFactory.getLogger(QuestionPaperController.class);
    
    @RequestMapping(value = "/qps", method = RequestMethod.POST)
    private ResponseEntity<?> create(
                                    @RequestParam("id") String id,
                                    @RequestParam("subject") String subject,
                                    @RequestParam("setId") String setId,
                                    @RequestParam("calendarYear") String calendarYear,
                                    @RequestParam("academicYear") String academicYear,
                                    @RequestParam("semester") String semester
                                ) throws IllegalStateException, IOException, Exception
    {
        
        if(setId != null  && calendarYear != null && academicYear != null && semester != null)
        {
            logger.info("Subject : {}", subject);
            
            QuestionPaper questionPaper = new QuestionPaper();
            questionPaper.setId(id);
            questionPaper.setSetId(setId);
            questionPaper.setCalendarYear(calendarYear);
            questionPaper.setAcademicYear(academicYear);
            questionPaper.setSemester(semester);
            
            QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();

            QuestionPaper createdQuestionPaper = questionPaperDelegate.create(subject, questionPaper);
            logger.info("Created question paper: {}", createdQuestionPaper);
            return new ResponseEntity(createdQuestionPaper, HttpStatus.CREATED);
        }
        else
        {
            logger.error("Unable to create. Set all the required fields.");
            CustomError customError = new CustomError();
            customError.addErrorMessage("Unable to create. Set all the required fields.");
            return new ResponseEntity(customError.getErrorMessages(), HttpStatus.BAD_REQUEST);
        } 
    }
    
    /*
     * If URL http://localhost:8080/qps is used, then error occurrs
     * status: 405, error: Method Not Allowed 
     */
    
    @RequestMapping(value = "/qps/{subject}", method = RequestMethod.GET)
    private ResponseEntity<?> listQuestionpapers(@PathVariable("subject") String subject) throws Exception
    {
        logger.info("Fetching all multiple choice questions."); 
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        List<QuestionPaper> allQuestionPapers = questionPaperDelegate.getAllQuestionPapers(subject);
        logger.info("All questions for {} : {}", subject, allQuestionPapers);
        return new ResponseEntity<List<QuestionPaper>>(allQuestionPapers, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/qps/{subject}/{id}", method = RequestMethod.GET )
    private ResponseEntity<?> searchQuestionPaperById(@PathVariable("subject") String subject, 
                                                    @PathVariable("id") String id)  throws Exception
    {
        logger.info("Fetching question paper of {} with id {}.", subject, id);
    
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        List<QuestionPaper> questionPapers = questionPaperDelegate.getQuestionPaperById(subject, id);
    
        if(questionPapers == null)
        {
            logger.error("Question paper with id {} is not found.", id);
            CustomError customError = new CustomError();
            customError.addErrorMessage("Question paper with id " + id + " is not found.");
            return new ResponseEntity(customError.getErrorMessages(), HttpStatus.NOT_FOUND);
        }
        else
        {
            logger.info("Question with id {} is : {}", id, questionPapers);
            return new ResponseEntity<List<QuestionPaper>>(questionPapers, HttpStatus.OK);
        }
    }
    
    @RequestMapping(value = "/qps/{subject}/question", params = "phrase", method = RequestMethod.GET )
    private ResponseEntity<?> searchQuestionPapersContainingQuestionsContainingPhrase(@PathVariable("subject") String subject,
                                                @RequestParam("phrase") String phrase)  throws Exception
    {
        logger.info("Fetching question papers {} having phrase {} in questions .", subject, phrase);
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        List<QuestionPaper> questionPapersContainingPhrase = questionPaperDelegate.getQuestionPapersContainingQuestionsContainingPhrase(subject, phrase);
        logger.info("Questions for {} having phrase {} are: ", subject, phrase);
        return new ResponseEntity<List<QuestionPaper>>(questionPapersContainingPhrase, HttpStatus.OK);
    }
    
    @RequestMapping (value = "/qps/{subject}/{id}", method = RequestMethod.PUT)
    private ResponseEntity<?> updateQuestionPaper(@PathVariable("subject") String subject, 
                            @PathVariable("id") String id , 
                            @RequestParam("setId") String setId,
                            @RequestParam("calendarYear") String calendarYear,
                            @RequestParam("academicYear") String academicYear,
                            @RequestParam("semester") String semester) throws IOException, Exception
    {
        logger.info("Updating question paper of {} with id {}.", subject, id);
        QuestionPaper questionPaper = getQuestionPaper(setId, calendarYear, academicYear, semester);
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        QuestionPaper updatedQuestionPaper = questionPaperDelegate.updateQuestion(subject, questionPaper);
        logger.info("Updated multiple choice questions : {}",updatedQuestionPaper);
        return new ResponseEntity<QuestionPaper>(updatedQuestionPaper, HttpStatus.OK);
    } 
    
    @RequestMapping(value = "/qps/{subject}", method = RequestMethod.DELETE)
    private ResponseEntity<?> deleteAllQuestionPapers(@PathVariable("subject") String subject) throws Exception
    {
        logger.info("Deleting all multiple choice questions.");
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        boolean areAllQuestionPapersDeleted = questionPaperDelegate.deleteAllQuestionPapers(subject);
        logger.info("Are all questions deleted ? : {}",areAllQuestionPapersDeleted);
        return new ResponseEntity<QuestionPaper>(HttpStatus.NO_CONTENT);
    }
    
    
    /*
     * When same URL is defined by different controllers within application, runtime exception is thrown.
     * 
     * Error creating bean with name 'requestMappingHandlerMapping' defined in class path resource 
     * [org/springframework/boot/autoconfigure/web/servlet/WebMvcAutoConfiguration$EnableWebMvcConfiguration.class]: 
     * Invocation of init method failed; nested exception is java.lang.IllegalStateException: 
     * Ambiguous mapping. Cannot map 'questionPaperController' method 
     * private org.springframework.http.ResponseEntity<?> org.spl.oes.qps.controllers.QuestionPaperController.deleteQuestionPaper
     * (java.lang.String,java.lang.String) throws java.lang.Exception to 
     * {DELETE /mcqs/{subject}/{id}}: There is already 'multipleChoiceQuestionController' bean method
     */
    
    @RequestMapping(value = "/qps/{subject}/{id}", method = RequestMethod.DELETE)
    private ResponseEntity<?> deleteQuestionPaper(@PathVariable("subject") String subject, @PathVariable("id") String id)
            throws Exception
    {   
        logger.info("Deleting multiple choice question for {} with id {}.", subject, id);
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        boolean isQuestionpaperDeleted = questionPaperDelegate.deleteQuestionPaper(subject, id);
        logger.info("Is question deleted ? : {}",isQuestionpaperDeleted);
        return new ResponseEntity<QuestionPaper>(HttpStatus.NO_CONTENT);
    }
    
    @RequestMapping(value = "/qps/{subject}/{id}/question/{questionId}", method = RequestMethod.PUT)
    private ResponseEntity<?> addQuestionToQuestionPaper(@PathVariable("subject") String subject,
                                                            @PathVariable("id") String id,
                                                            @PathVariable("questionId") String questionId
                        ) throws Exception
    {
        logger.info("Add question of subject {} and id {} to question paper with id {}.", subject, questionId, id);
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        QuestionPaper questionPaper = questionPaperDelegate.addMultipleChoiceQuestion(subject, id, questionId);
        return new ResponseEntity<QuestionPaper>(questionPaper, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/qps/{subject}/{id}/question/{questionId}", method = RequestMethod.DELETE)
    private ResponseEntity<?> deleteQuestionFromQuestionPaper(@PathVariable("subject") String subject,
                                                                @PathVariable("id") String id,
                                                                @PathVariable("questionId") String questionId
                                                    ) throws Exception
    {
        logger.info("Remove question of subject {} and id {} from question paper with id {}.", subject, questionId, id);
        QuestionPaperDelegate questionPaperDelegate = new QuestionPaperDelegate();
        QuestionPaper questionPaper = questionPaperDelegate.removeMultipleChoiceQuestion(subject, id, questionId);
        return new ResponseEntity<QuestionPaper>(questionPaper, HttpStatus.OK);
    }
    
    private QuestionPaper getQuestionPaper(String setId, String calendarYear, String academicYear, String semester ) 
    {
        QuestionPaper questionPaper = new QuestionPaper();
        questionPaper.setSetId(setId);
        questionPaper.setCalendarYear(calendarYear);
        questionPaper.setAcademicYear(academicYear);
        questionPaper.setSemester(semester);
        return questionPaper;
    }     
}
