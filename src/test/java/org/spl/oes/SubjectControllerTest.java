/**
 * 
 */
package org.spl.oes;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spl.oes.subs.controllers.SubjectController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author sane
 *
 */
//@RunWith(SpringRunner.class)
//@WebMvcTest(SubjectController.class)
//@ContextConfiguration(classes= {QuestionsApplication.class})
public class SubjectControllerTest
{
    private static final Logger logger = LoggerFactory.getLogger(SubjectControllerTest.class);
    
    @Autowired
    private MockMvc mvc;
    
//    @Test
//    public void create() throws Exception
//    {
//        String subject = TestUtil.getAlphaNumericString(30);
//        
//        logger.info("*** Test create subject ***");
//        
//        logger.info("subjet : {}", subject);
//        
//        mvc.perform(post("/subs")
//                .contentType(MediaType.MULTIPART_FORM_DATA)
//                .param("subject",subject))
//        .andExpect(status().isCreated())
//        .andExpect(content().string(subject)); 
//    }
    
}
